import bos.Counter;
public class CounterExample {
	static Counter counter = new Counter();
	

public static void main(String[] args) {

	counter.set(3);
	Thread a = new Thread(() -> counter.decrement());
	Thread b = new Thread(() -> counter.decrement());
	Thread c = new Thread(() -> counter.decrement());
	try {
		Thread.sleep(500);
	} catch (InterruptedException e) {
		e.printStackTrace();
	}
	System.out.println(counter.depleted());
}
}